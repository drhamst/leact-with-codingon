import React from 'react';
import style from './Mycss.module.css';
import Names from 'classnames/bind';

const setting = Names.bind(style);

const Mycss = () => {
  return (
    <div className={setting("box", "box2")}>
      <h1>CSS Applying</h1>
    </div>
  );
}

export default Mycss;