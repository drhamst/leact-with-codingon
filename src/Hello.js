import react, {Component} from "react";
import PropTypes from "prop-types";

class Hello extends Component {
  render() {
    const { hi, children } = this.props;
    
    return (
      <div>
        Hi, i'm {hi}
        <br />
        Children: {children}
      </div>
    );
  }
}
export default Hello;
