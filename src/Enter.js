import react, { Component } from "react";

class Enter extends Component {
  state = {
    enter: 0
  };

  render() {
    const { enter } = this.state;

    return (
      <div>
        <h1>Entered Customer: {enter} Customers</h1>

        <button
          onClick={() => {
            this.setState({ enter: enter + 1 });
          }}>
          Enter
        </button>
      </div>
    );
  }

}

export default Enter;