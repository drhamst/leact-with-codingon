import React, { useEffect, useState } from 'react';

const Info = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const onChangeName = (e) => {
    setName(e.target.value);
  }

  const onChangeEmail = (e) => {
    setEmail(e.target.value);
  }

  useEffect(() => {
    console.log('Rendering Complete');
  },[name]);

  return (
    <div>
      <div>
        <h2>Name: {name}</h2>
        <h2>Email: {email}</h2>
      </div>
      <div>
        <input value={name} onChange={onChangeName} />
        <input value={email} onChange={onChangeEmail} />
      </div>

    </div>
  );
};

export default Info;