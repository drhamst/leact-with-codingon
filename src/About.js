import React from 'react';
import qs from 'qs';

const About = ({ location }) => {
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true // Skip ? mark
  });

  const showDetail = query.detail ==="true" // 쿼리 파싱 결과값은 문자열
  
  return (
    <div>
      <h1>About</h1>
      <p>This is About page</p>

      { showDetail && <p>detail true!!</p>}
    </div>
  );
};

export default About;