import react, { useState } from 'react';

const Event = () => {
  const [username, setUsername] = useState("");
  const [message, setMessage] = useState("");
  const onChangeUsername = (e) => setUsername(e.target.value);
  const onChangeMessage = (e) => setMessage(e.target.value);

  const onClick = () => {
    alert(username + ': ' + message);
    setUsername("");
    setMessage("");
  }

  const onKeyPress = (e) => {
    if (e.key === 'Enter') {
      onClick();
    }
  }

  return (
    <div>
      <h1>React's Events</h1>

      <input
        type="text"
        name="username"
        placeholder="username"
        value={username}
        onChange={onChangeUsername}
      ></input>
      <input
        type="text"
        name="message"
        placeholder="message here"
        value={message}
        onChange={onChangeMessage}
        onKeyPress={onKeyPress}
      ></input>

      <button onClick={onClick}>Click</button>
    </div>
  );
};

export default Event;