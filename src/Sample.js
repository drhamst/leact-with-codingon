import React from 'react';
import react, { useState } from 'react';

const Sample = () => {
  const [names, setNames] = useState([
    { id: 1, text: "Jean" },
    { id: 2, text: "Shirts" },
    { id: 3, text: "Boots" },
    { id: 4, text: "Jacket" },
  ]);

  const [inputText, setInputText] = useState("");
  const [nextId, setNextId] = useState(5);

  const namesList = names.map(name => <li key={name.id} onDoubleClick={() => onRemove(name.id)}> {name.text}</li>);

  const onChange = (e) => setInputText(e.target.value);

  const onClick = () => {
    const nextNames = names.concat({
      id: nextId,
      text: inputText
    })
    setNextId(nextId + 1);
    setNames(nextNames);
    setInputText("");
  }

  const onRemove = id => {
    const nextNames = names.filter(name => name.id !== id);
    setNames(nextNames);
  }

  return (
    <div>
      <input value={inputText} onChange={onChange}></input>
      <button onClick={onClick}>Add</button>
      <ul>
        {namesList}
      </ul>
    </div>

  );
};

export default Sample;