import React from 'react';

const data = {
  roy: {
    name: "roy",
    desc: "Hate shower"
  },
  coco: {
    name: "coco",
    desc: "Love play"
  }
}

const NewInfo = ({ match }) => {

  const { username } = match.params;
  const info = data[username];

  if (!info) {
    return <div>There is no name</div>
  }

  return (
    <div>
      <h2>
        {username} ({info.name})
      </h2>

      <p>
        {info.desc}
      </p>

    </div>
  );
};

export default NewInfo;