import react, { useState } from "react";

const Lan = () => {
  const [msg, msg_change] = useState("");
  const KorEnter = () => msg_change("This is korean");
  const EngEnter = () => msg_change("This is english");

  return (
    <div>
      <button onClick={KorEnter}>Korean</button>
      <button onClick={EngEnter}>English</button>

      <h1>{msg}</h1>
    </div>
  );
}

export default Lan;