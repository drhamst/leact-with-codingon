import React, { useState, useRef, useCallback } from 'react';
import TodoInsert from './components/TodoInsert';
import TodoTemplate from './components/TodoTemplate';
import TodoList from './components/TodoList'

const App = () => {
  const [todos, setTodos] = useState([
    {
      id: 1,
      text: "React Fundamental",
      checked: true
    },
    {
      id: 2,
      text: "Drinking Coffee",
      checked: true
    },
    {
      id: 3,
      text: "Walk",
      checked: false
    },
    {
      id: 4,
      text: "Take a breath",
      checked: false
    },
  ]);

  const nextId = useRef(5);

  const onInsert = useCallback(
    text => {
      const todo = {
        id: nextId.current,
        text,
        checked: false
      };

      setTodos(todos.concat(todo));
      nextId.current += 1;
    }, [todos]
  );

  const onRemove = useCallback(
    (id) => {
      setTodos(todos.filter(todo => todo.id !== id));
    },
    [todos]
  );

  const onToggle = useCallback(
    (id) => {
      setTodos(
        todos.map(
          todo =>
            todo.id === id ? { ...todo, checked: !todo.checked } : todo
        )
      );
    },
    [todos],
  )

  return (
    <TodoTemplate>
      <TodoInsert onInsert={onInsert} />
      <TodoList todos={todos} onRemove={onRemove} onToggle={onToggle} />
    </TodoTemplate>
  );
}

export default App;